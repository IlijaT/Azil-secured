package mirkovic.azil.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mirkovic.azil.model.Pas;
import mirkovic.azil.service.PasService;
import mirkovic.azil.support.PasDTOToPas;
import mirkovic.azil.support.PasToPasDTO;
import mirkovic.azil.web.dto.PasDTO;

@RestController
@RequestMapping(value= "/api/psi")
public class ApiPasController {
	
	@Autowired
	PasService pasService;
	
	@Autowired
	PasDTOToPas toPas;
	
	@Autowired
	PasToPasDTO toDto;
	
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<PasDTO>> get(
			@RequestParam(required = false) String rasa,
			@RequestParam(required = false) Double starost, 
			@RequestParam(required = false) String pol,
			@RequestParam(required = false) Long azilId,
			@RequestParam(defaultValue = "0") int pageNum) {
		
		
		
		Page<Pas> kerovi;
		
		if (rasa != null || starost != null || pol != null || azilId != null) {
			kerovi = pasService.pretraga(rasa, starost, pol, azilId, pageNum);
		} else {
			kerovi = pasService.findAll(pageNum);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(kerovi.getTotalPages()));
		return new ResponseEntity<>(toDto.convert(kerovi.getContent()), headers, HttpStatus.OK);
		

 

		
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<PasDTO> get(@PathVariable Long id) {

		Pas pas = pasService.findOne(id);

		if (pas == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDto.convert(pas), HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<PasDTO> add(@RequestBody PasDTO noviPas) {

		Pas pas = toPas.convert(noviPas);
		pasService.save(pas);

		return new ResponseEntity<>(toDto.convert(pas), HttpStatus.CREATED);
	}
	

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public ResponseEntity<PasDTO> edit(@PathVariable Long id, @RequestBody PasDTO zaIzmenu) {

		if (!id.equals(zaIzmenu.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		
		Pas persisted = pasService.save(toPas.convert(zaIzmenu));

		return new ResponseEntity<>(toDto.convert(persisted), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<PasDTO> delete(@PathVariable Long id) {

		
		pasService.remove(id);
		
		return new ResponseEntity<>( HttpStatus.NO_CONTENT);
	}
}
