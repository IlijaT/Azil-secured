package mirkovic.azil.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Azil {
	@Id
	@GeneratedValue
	@Column(name="id")
	private Long id;
	private String ime;
	private String grad;
	private String adresa;
	private String telefon;
	
	@OneToMany(mappedBy="azil", cascade=CascadeType.REMOVE)
	private List<Pas> psi = new ArrayList<>();
	
	@OneToMany(mappedBy="azil", cascade=CascadeType.REMOVE)
	private List<Macka> macke = new ArrayList<>();
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	


}
