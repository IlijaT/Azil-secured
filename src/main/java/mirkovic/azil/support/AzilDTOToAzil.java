package mirkovic.azil.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import mirkovic.azil.model.Azil;
import mirkovic.azil.service.AzilService;
import mirkovic.azil.web.dto.AzilDTO;

@Component
public class AzilDTOToAzil implements Converter<AzilDTO, Azil> {

	@Autowired
	private AzilService azilService;
	
	@Override
	public Azil convert(AzilDTO dto) {
		Azil azil = new Azil();
		
		if(dto.getId() != null) {
			azil = azilService.findOne(dto.getId());
		}
		
		azil.setId(dto.getId());
		azil.setGrad(dto.getGrad());
		azil.setIme(dto.getIme());
		azil.setAdresa(dto.getAdresa());
		azil.setTelefon(dto.getTelefon());
		
		return azil;
	}

	 

}
