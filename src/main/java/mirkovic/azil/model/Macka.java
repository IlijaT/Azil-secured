package mirkovic.azil.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Macka {
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private Long id;
	private String ime;
	private String pol;
	private Double starost;
	private String rasa;
	private Double tezina;
	private String slika;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Azil azil;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public Double getStarost() {
		return starost;
	}
	public void setStarost(Double starost) {
		this.starost = starost;
	}
	public String getRasa() {
		return rasa;
	}
	public void setRasa(String rasa) {
		this.rasa = rasa;
	}
	public Double getTezina() {
		return tezina;
	}
	public void setTezina(Double tezina) {
		this.tezina = tezina;
	}
	public String getSlika() {
		return slika;
	}
	public void setSlika(String slika) {
		this.slika = slika;
	}
	public Azil getAzil() {
		return azil;
	}
	public void setAzil(Azil azil) {
		this.azil = azil;
	}
	
	
	
	
	
}
