package mirkovic.azil.service;


import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import mirkovic.azil.model.Macka;

public interface MackaService {
	Macka findOne(Long id);
	Page<Macka> findAll(int pageNum);
	Macka save(Macka macka); 
	void remove(Long id);
	
 

	Page<Macka> pretraga(
			@Param("rasa") String rasa, 
			@Param("starost") Double starost, 
			@Param("pol") String pol, 
			@Param("azilId") Long azilId, 
			int page);
	
}
