package mirkovic.azil.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import mirkovic.azil.model.Macka;
import mirkovic.azil.web.dto.MackaDTO;

@Component
public class MackaToMackaDTO implements Converter<Macka, MackaDTO> {

	@Override
	public MackaDTO convert(Macka source) {
		MackaDTO mackaDTO = new MackaDTO();
		
		mackaDTO.setId(source.getId());
		mackaDTO.setIme(source.getIme());
		mackaDTO.setPol(source.getPol());
		mackaDTO.setRasa(source.getRasa());
		mackaDTO.setSlika(source.getSlika());
		mackaDTO.setStarost(source.getStarost());
		mackaDTO.setTezina(source.getTezina());
		mackaDTO.setAzilId(source.getAzil().getId());
		mackaDTO.setAzilNaziv(source.getAzil().getIme());
		
		
		return mackaDTO;
	}
	
	
	public List<MackaDTO> convert(List<Macka> macketine){
		List<MackaDTO> ret = new ArrayList<>();
		
		for(Macka x : macketine){
			ret.add(convert(x));
		}
		
		return ret;

	}
}
