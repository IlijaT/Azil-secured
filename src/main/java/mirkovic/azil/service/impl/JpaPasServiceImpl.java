package mirkovic.azil.service.impl;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import mirkovic.azil.model.Pas;
import mirkovic.azil.repository.PasRepository;
import mirkovic.azil.service.PasService;

@Service
@Transactional
public class JpaPasServiceImpl implements PasService {
	
	@Autowired
	private PasRepository pasRepository;
	
	@Override
	public Pas findOne(Long id) {
		return pasRepository.findOne(id);
	}

	 

	@Override
	public Pas save(Pas pas) {
		return pasRepository.save(pas);
	}

	@Override
	public void remove(Long id) {
		pasRepository.delete(id);

	}

	@Override
	public Page<Pas> findAll(int pageNum) {
		return pasRepository.findAll(new PageRequest(pageNum, 10) );
	}



	@Override
	public Page<Pas> pretraga(String rasa, Double starost, String pol, Long azilId, int page) {
		return pasRepository.pretraga(rasa, starost, pol, azilId, new PageRequest(page, 10));
	}

	 

}
