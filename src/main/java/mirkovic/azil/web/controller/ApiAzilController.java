package mirkovic.azil.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mirkovic.azil.model.Azil;
import mirkovic.azil.service.AzilService;
import mirkovic.azil.support.AzilDTOToAzil;
import mirkovic.azil.support.AzilToAzilDTO;
import mirkovic.azil.web.dto.AzilDTO;

@RestController
@RequestMapping(value= "/api/azili")
public class ApiAzilController {
	
	@Autowired
	AzilService azilService;
	
	@Autowired
	AzilToAzilDTO toDto;
	
	@Autowired
	AzilDTOToAzil toAzil;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<AzilDTO>> get( ){
		
		List<Azil> azili = azilService.findAll( );
		
		return new ResponseEntity<>(
				toDto.convert(azili),
				HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<AzilDTO> get(@PathVariable Long id) {

		Azil azil = azilService.findOne(id);

		if (azil == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDto.convert(azil), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<AzilDTO> add(@RequestBody AzilDTO noviAzil) {

		Azil azil = toAzil.convert(noviAzil);
		azilService.save(azil);

		return new ResponseEntity<>(toDto.convert(azil), HttpStatus.CREATED);
	}

	
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public ResponseEntity<AzilDTO> edit(@PathVariable Long id, @RequestBody AzilDTO izmenjen) {

		if (!id.equals(izmenjen.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		
		Azil persisted = azilService.save(toAzil.convert(izmenjen));

		return new ResponseEntity<>(toDto.convert(persisted), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<AzilDTO> delete(@PathVariable Long id) {

		
		azilService.remove(id);
		
		return new ResponseEntity<>( HttpStatus.NO_CONTENT);
	}
	
}
