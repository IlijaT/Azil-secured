package mirkovic.azil.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mirkovic.azil.model.Macka;

@Repository
public interface MackaRepository extends JpaRepository<Macka, Long> {

	
	@Query("SELECT m FROM Macka m WHERE m.rasa = :rasa")
	public Macka findByRasa(@Param("rasa") String rasa);
	
	
	
	@Query("SELECT m FROM Macka m WHERE "
			+ "(:rasa IS NULL or m.rasa LIKE CONCAT('%', :rasa, '%') ) AND "
			+ "(:starost IS NULL OR m.starost <= :starost) AND "
			+ "(:pol IS NULL or m.pol LIKE CONCAT('%', :pol, '%') ) AND "
			+ "(:azilId IS NULL or m.azil.id = :azilId ) "
			)
	Page<Macka> pretraga(
			@Param("rasa") String rasa, 
			@Param("starost") Double starost, 
			@Param("pol") String pol, 
			@Param("azilId") Long azilId, 
			Pageable pageRequest);
}
