package mirkovic.azil.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import mirkovic.azil.model.Pas;
import mirkovic.azil.service.AzilService;
import mirkovic.azil.service.PasService;
import mirkovic.azil.web.dto.PasDTO;

@Component
public class PasDTOToPas implements Converter<PasDTO, Pas> {
	
	@Autowired
	private AzilService azilService;
	
	@Autowired
	private PasService pasService;
	
	@Override
	public Pas convert(PasDTO source) {
		
		Pas pas = new Pas();
		
		if(source.getId() != null) {
			pas = pasService.findOne(source.getId());
			
			if(pas == null) {
				throw new  IllegalStateException("Id psa ne postoji");
			}
		}
		
		pas.setId(source.getId());
		pas.setIme(source.getIme());
		pas.setPol(source.getPol());
		pas.setRasa(source.getRasa());
		pas.setSlika(source.getSlika());
		pas.setStarost(source.getStarost());
		pas.setAzil(azilService.findOne(source.getAzilId()));
		pas.setTezina(source.getTezina());
		
		return pas;
	}

}
