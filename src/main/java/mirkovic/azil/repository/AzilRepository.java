package mirkovic.azil.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mirkovic.azil.model.Azil;

@Repository
public interface AzilRepository extends JpaRepository<Azil, Long> {

}
