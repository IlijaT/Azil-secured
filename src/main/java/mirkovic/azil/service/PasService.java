package mirkovic.azil.service;


import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import mirkovic.azil.model.Pas;

public interface PasService {
	
	Pas findOne(Long id);
	Page<Pas> findAll(int pageNum);
	Pas save(Pas pas); 
	void remove(Long id);
	
	Page<Pas> pretraga(
			@Param("rasa") String rasa, 
			@Param("starost") Double starost, 
			@Param("pol") String pol, 
			@Param("azilId") Long azilId, 
			int page);
	
}
