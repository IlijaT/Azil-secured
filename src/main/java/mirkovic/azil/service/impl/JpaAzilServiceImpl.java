package mirkovic.azil.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mirkovic.azil.model.Azil;
import mirkovic.azil.repository.AzilRepository;
import mirkovic.azil.service.AzilService;

@Service
@Transactional
public class JpaAzilServiceImpl implements AzilService {
	
	@Autowired
	private AzilRepository azilRepository;
	
	
	
	@Override
	public Azil findOne(Long id) {
		return azilRepository.findOne(id);
	}

	@Override
	public List<Azil> findAll() {
		return azilRepository.findAll();
	}

	@Override
	public Azil save(Azil azil) {
		return azilRepository.save(azil);
	}

	@Override
	public void remove(Long id) {
		azilRepository.delete(id);

	}

}
