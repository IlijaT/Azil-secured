package mirkovic.azil.web.dto;

public class MackaDTO {
	
	private Long id;
	private String ime;
	private String pol;
	private Double starost;
	private String rasa;
	private Double tezina;
	private String slika;
	private Long azilId;
	private String azilNaziv;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public Double getStarost() {
		return starost;
	}
	public void setStarost(Double starost) {
		this.starost = starost;
	}
	public String getRasa() {
		return rasa;
	}
	public void setRasa(String rasa) {
		this.rasa = rasa;
	}
	public Double getTezina() {
		return tezina;
	}
	public void setTezina(Double tezina) {
		this.tezina = tezina;
	}
	public String getSlika() {
		return slika;
	}
	public void setSlika(String slika) {
		this.slika = slika;
	}
	public Long getAzilId() {
		return azilId;
	}
	public void setAzilId(Long azilId) {
		this.azilId = azilId;
	}
	public String getAzilNaziv() {
		return azilNaziv;
	}
	public void setAzilNaziv(String azilNaziv) {
		this.azilNaziv = azilNaziv;
	}
	
	

}
