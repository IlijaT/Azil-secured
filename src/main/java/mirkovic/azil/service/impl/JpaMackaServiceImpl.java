package mirkovic.azil.service.impl;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import mirkovic.azil.model.Macka;
import mirkovic.azil.repository.MackaRepository;
import mirkovic.azil.service.MackaService;

@Service
@Transactional
public class JpaMackaServiceImpl implements MackaService {
	
	@Autowired
	MackaRepository mackaRepository;
	 
	@Override
	public Macka findOne(Long id) {
		return mackaRepository.findOne(id);
	}

	

	@Override
	public Macka save(Macka macka) {
		return mackaRepository.save(macka);
	}

	@Override
	public void remove(Long id) {
		mackaRepository.delete(id);

	}

	@Override
	public Page<Macka> findAll(int pageNum) {
		return mackaRepository.findAll(new PageRequest(pageNum, 10) );
	}



	@Override
	public Page<Macka> pretraga(String rasa, Double starost, String pol, Long azilId, int page) {
		return mackaRepository.pretraga(rasa, starost, pol, azilId, new PageRequest(page, 10));
	}


}
