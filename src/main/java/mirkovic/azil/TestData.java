package mirkovic.azil;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mirkovic.azil.model.Azil;
import mirkovic.azil.model.Macka;
import mirkovic.azil.model.Pas;
import mirkovic.azil.service.AzilService;
import mirkovic.azil.service.MackaService;
import mirkovic.azil.service.PasService;


@Component
public class TestData {
	
	@Autowired
	private AzilService azilService;
	
	@Autowired
	private PasService pasService;
	
	@Autowired
	private MackaService mackaService;
	
	@PostConstruct
	public void init(){ 
		
		Azil a1 = new Azil();
		a1.setIme("Mirkovic Azil - Novi Sad");
		a1.setAdresa("Mileve Maric 5");
		a1.setGrad("Novi Sad");
		a1.setTelefon("021 25 555");
		azilService.save(a1);
		
		Macka m1 = new Macka();
		m1.setId(1L);
		m1.setIme("Plavi");
		m1.setPol("Muski");
		m1.setRasa("Ruska siva macka");
		m1.setSlika("/assets/img/macke/cat1.jpg");
		m1.setStarost(5.0);
		m1.setTezina(8.2);
		m1.setAzil(a1);
		 
		mackaService.save(m1);
		
		Macka m2 = new Macka();
		m2.setIme("Sladja");
		m2.setPol("Zenski");
		m2.setRasa("Domaca");
		m2.setSlika("/assets/img/macke/domaca1.jpg");
		m2.setStarost(2.0);
		m2.setTezina(6.2);
		m2.setAzil(a1);
		 
		mackaService.save(m2);
		
		Macka m3 = new Macka();
		m3.setIme("Persa");
		m3.setPol("Zenski");
		m3.setRasa("Persijska");
		m3.setSlika("/assets/img/macke/persijska1.jpg");
		m3.setStarost(1.6);
		m3.setTezina(7.2);
		m3.setAzil(a1);
		 
		mackaService.save(m3);
		
		Pas p1 = new Pas();
		p1.setId(1L);
		p1.setIme("Aki");
		p1.setPol("Muski");
		p1.setRasa("Aljaski malamut");
		p1.setSlika("/assets/img/psi/dog1.jpg");
		p1.setStarost(1.8);
		p1.setTezina(34.2);
		p1.setAzil(a1);
		 
		pasService.save(p1);
		
		Pas p2 = new Pas();
		p2.setIme("Dzonson");
		p2.setPol("Muski");
		p2.setRasa("Mesanac");
		p2.setSlika("/assets/img/psi/mesanac2.jpg");
		p2.setStarost(8.2);
		p2.setTezina(16.2);
		p2.setAzil(a1);
		 
		pasService.save(p2);
		
		Pas p3 = new Pas();
		p3.setIme("Kifla");
		p3.setPol("Muski");
		p3.setRasa("Jazavicar");
		p3.setSlika("/assets/img/psi/dog2.jpg");
		p3.setStarost(5.2);
		p3.setTezina(13.2);
		p3.setAzil(a1);
		 
		pasService.save(p3);
		
		Pas p4 = new Pas();
		p4.setIme("Kiki");
		p4.setPol("Zenski");
		p4.setRasa("Labrador");
		p4.setSlika("/assets/img/psi/labrador1.jpg");
		p4.setStarost(0.2);
		p4.setTezina(1.1);
		p4.setAzil(a1);
		 
		pasService.save(p4);
		 
		
		 
	}
	 
}
