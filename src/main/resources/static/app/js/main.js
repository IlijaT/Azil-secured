var azilApp = angular.module('azilApp', ['ngRoute']);

azilApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : '/app/html/partial/home.html'
		})
		.when('/macke', {
			templateUrl : '/app/html/partial/macke.html'
		})
		.when('/psi', {
			templateUrl : '/app/html/partial/psi.html'
		})
		.when('/volontiraj', {
			templateUrl : '/app/html/partial/volontiraj.html'
		})
		.when('/doniraj', {
			templateUrl : '/app/html/partial/doniraj.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);


azilApp.controller("psiCtrl", function($scope, $http, $location){
	
	var baseUrl = "/api/psi";
	
	$scope.psi = [];
	$scope.azili = [];
 
	$scope.trazeniPas = {};
	$scope.trazeniPas.rasa= "";
	$scope.trazeniPas.starost = "";
	$scope.trazeniPas.pol = "";
	$scope.trazeniPas.azilId = "";
	
	$scope.pageNum = 0;
	$scope.totalPages = 1;
	
	var getPsi = function(){
		
		var config = { params: {}};
		
		if($scope.trazeniPas.rasa != ""){
			config.params.rasa = $scope.trazeniPas.rasa;
		}
		if($scope.trazeniPas.starost != ""){
			config.params.starost = $scope.trazeniPas.starost;
		}
		if($scope.trazeniPas.pol != ""){
			config.params.pol = $scope.trazeniPas.pol;
		}
		if($scope.trazeniPas.azilId != ""){
			config.params.azilId = $scope.trazeniPas.azilId;
		}
		
		config.params.pageNum = $scope.pageNum;
		
		var promise = $http.get(baseUrl, config);
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.psi = data.data;
					$scope.trazeniPas = {};
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getPsi();
	
	var getAzili = function(){
		
		var promise = $http.get("/api/azili");
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.azili = data.data;
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getAzili();
	
	$scope.page = function(direction){
		$scope.pageNum = $scope.pageNum + direction;
		getPsi();
	}
	
	
	$scope.filtriranje = function(){
		$scope.pageNum = 0;
		getPsi();
	}
	
});


azilApp.controller("mackeCtrl", function($scope, $http, $location){
	
	var baseUrl = "/api/macke";
	
	$scope.macke = [];
	$scope.azili = [];
 
	$scope.trazenaMacka = {};
	$scope.trazenaMacka.rasa= "";
	$scope.trazenaMacka.starost = "";
	$scope.trazenaMacka.pol = "";
	$scope.trazenaMacka.azilId = "";
	
	$scope.pageNum = 0;
	$scope.totalPages = 1;
	
	var getMacke = function(){
		
		var config = { params: {}};
		
		if($scope.trazenaMacka.rasa != ""){
			config.params.rasa = $scope.trazenaMacka.rasa;
		}
		if($scope.trazenaMacka.starost != ""){
			config.params.starost = $scope.trazenaMacka.starost;
		}
		if($scope.trazenaMacka.pol != ""){
			config.params.pol = $scope.trazenaMacka.pol;
		}
		if($scope.trazenaMacka.azilId != ""){
			config.params.azilId = $scope.trazenaMacka.azilId;
		}
		
		config.params.pageNum = $scope.pageNum;
		
		var promise = $http.get(baseUrl, config);
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.macke = data.data;
					$scope.trazenaMacka = {};
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getMacke();
	
	var getAzili = function(){
		
		var promise = $http.get("/api/azili");
		promise.then(
				function uspeh(data){
					$scope.totalPages = data.headers("totalPages");
					$scope.azili = data.data;
				},
				function neuspeh(data){
					alert("Neuspesno dobavljanje podataka.");
				}
		);
	}
	
	getAzili();
	
	$scope.page = function(direction){
		$scope.pageNum = $scope.pageNum + direction;
		getMacke();
	}
	
	
	$scope.filtriranje = function(){
		$scope.pageNum = 0;
		getMacke();
	}
	
});

