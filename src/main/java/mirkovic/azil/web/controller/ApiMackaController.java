package mirkovic.azil.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mirkovic.azil.model.Macka;
import mirkovic.azil.service.MackaService;
import mirkovic.azil.support.MackaDTOToMacka;
import mirkovic.azil.support.MackaToMackaDTO;
import mirkovic.azil.web.dto.MackaDTO;

@RestController
@RequestMapping(value= "/api/macke")
public class ApiMackaController {
	
	@Autowired	
	MackaService mackaService;
	
	
	@Autowired
	private MackaDTOToMacka toMacka;

	@Autowired
	private MackaToMackaDTO toDTO;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<MackaDTO>> get(
			@RequestParam(required = false) String rasa,
			@RequestParam(required = false) Double starost, 
			@RequestParam(required = false) String pol,
			@RequestParam(required = false) Long azilId,
			@RequestParam(defaultValue = "0") int pageNum) {
		
 

		Page<Macka> macke;
		
		if (rasa != null || starost != null || pol != null || azilId != null) {
			macke = mackaService.pretraga(rasa, starost, pol, azilId, pageNum);
		} else {
			macke = mackaService.findAll(pageNum);
		}
				
				

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(macke.getTotalPages()) );
		
		return new ResponseEntity<>(toDTO.convert(macke.getContent()), headers, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<MackaDTO> get(@PathVariable Long id) {

		Macka macak = mackaService.findOne(id);

		if (macak == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDTO.convert(macak), HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<MackaDTO> add(@RequestBody MackaDTO novaMacka) {

		Macka macka = toMacka.convert(novaMacka);
		mackaService.save(macka);

		return new ResponseEntity<>(toDTO.convert(macka), HttpStatus.CREATED);
	}

	
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public ResponseEntity<MackaDTO> edit(@PathVariable Long id, @RequestBody MackaDTO zaIzmenu) {

		if (!id.equals(zaIzmenu.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		
		Macka persisted = mackaService.save(toMacka.convert(zaIzmenu));

		return new ResponseEntity<>(toDTO.convert(persisted), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {

		
		mackaService.remove(id);
		
		return new ResponseEntity<>( HttpStatus.NO_CONTENT);
	}
}
