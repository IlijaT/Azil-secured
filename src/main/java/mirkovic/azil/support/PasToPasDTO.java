package mirkovic.azil.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import mirkovic.azil.model.Pas;
import mirkovic.azil.web.dto.PasDTO;

@Component
public class PasToPasDTO implements Converter<Pas, PasDTO> {

	@Override
	public PasDTO convert(Pas source) {
		PasDTO pasDTO = new PasDTO();
		
		pasDTO.setId(source.getId());
		pasDTO.setIme(source.getIme());
		pasDTO.setPol(source.getPol());
		pasDTO.setRasa(source.getRasa());
		pasDTO.setSlika(source.getSlika());
		pasDTO.setStarost(source.getStarost());
		pasDTO.setTezina(source.getTezina());
		pasDTO.setAzilId(source.getAzil().getId());
		pasDTO.setAzilNaziv(source.getAzil().getIme());
		
		
		return pasDTO;
	}
	
	
	public List<PasDTO> convert(List<Pas> kerovi){
		List<PasDTO> ret = new ArrayList<>();
		
		for(Pas x : kerovi){
			ret.add(convert(x));
		}
		
		return ret;

	}
}
