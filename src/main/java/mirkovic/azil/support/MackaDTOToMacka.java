package mirkovic.azil.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import mirkovic.azil.model.Macka;
import mirkovic.azil.service.AzilService;
import mirkovic.azil.service.MackaService;
import mirkovic.azil.web.dto.MackaDTO;

@Component
public class MackaDTOToMacka implements Converter<MackaDTO, Macka> {
	
	@Autowired
	private AzilService azilService;
	
	@Autowired
	private MackaService mackaService;
	
	
	@Override
	public Macka convert(MackaDTO source) {
		
		Macka macka = new Macka();
		
		if(source.getId() != null) {
			macka = mackaService.findOne(source.getId());
			
			if(macka == null) {
				throw new  IllegalStateException("Id macke ne postoji");
			}
		}
		
		macka.setId(source.getId());
		macka.setIme(source.getIme());
		macka.setPol(source.getPol());
		macka.setRasa(source.getRasa());
		macka.setSlika(source.getSlika());
		macka.setStarost(source.getStarost());
		macka.setTezina(source.getTezina());
		macka.setAzil(azilService.findOne(source.getAzilId()));
		
		
		
		return macka;
	}

}
