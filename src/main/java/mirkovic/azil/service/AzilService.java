package mirkovic.azil.service;

import java.util.List;

import mirkovic.azil.model.Azil;

public interface AzilService {

	
	Azil findOne(Long id);
	List<Azil> findAll();
	Azil save(Azil azil); 
	void remove(Long id);
}
