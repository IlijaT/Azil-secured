package mirkovic.azil.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mirkovic.azil.model.Pas;

@Repository
public interface PasRepository extends JpaRepository<Pas, Long> {
	
	@Query("SELECT p FROM Pas p WHERE p.rasa = :rasa")
	public Pas findByRasa(@Param("rasa") String rasa);

	@Query("SELECT p FROM Pas p WHERE "
			+ "(:rasa IS NULL or p.rasa LIKE CONCAT('%', :rasa, '%') ) AND "
			+ "(:starost IS NULL OR p.starost <= :starost) AND "
			+ "(:pol IS NULL or p.pol LIKE CONCAT('%', :pol, '%') ) AND "
			+ "(:azilId IS NULL or p.azil.id = :azilId ) "
			)
	Page<Pas> pretraga(
			@Param("rasa") String rasa, 
			@Param("starost") Double starost, 
			@Param("pol") String pol, 
			@Param("azilId") Long azilId, 
			Pageable pageRequest);

	
	
 
}
