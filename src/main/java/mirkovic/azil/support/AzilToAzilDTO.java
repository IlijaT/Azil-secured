package mirkovic.azil.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import mirkovic.azil.model.Azil;
import mirkovic.azil.web.dto.AzilDTO;

@Component
public class AzilToAzilDTO implements Converter<Azil, AzilDTO> {

	@Override
	public AzilDTO convert(Azil azil) {
		
		AzilDTO dto = new AzilDTO();
		
		dto.setId(azil.getId());
		dto.setGrad(azil.getGrad());
		dto.setAdresa(azil.getAdresa());
		dto.setIme(azil.getIme());
		dto.setTelefon(azil.getTelefon());
		
		
		return dto;
	}
	
	public List<AzilDTO> convert(List<Azil> lista){
		
		List<AzilDTO> novaLista = new ArrayList<>();
		
		for(Azil a : lista) {
			
			novaLista.add(convert(a));
			
		}
		
		return novaLista;
		
	}

}
